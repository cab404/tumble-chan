use serenity::client::EventHandler;
use serenity::Client;
use serenity::{model::channel::Message, prelude::Context};

struct MainHandler;

impl EventHandler for MainHandler {
    fn message(&self, ctx: Context, _new_message: Message) {
        println!("A message! {}", _new_message.content)
    }
}

fn main() {
    let mut client = Client::new("insert_token_here", MainHandler).unwrap();
    println!("Hello!");
    client.start().unwrap();
}
